<?php

/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
*
* @author larry
**/ 
Route::get('event',function(){
 
    return "This is from events";
    //return Event::index();  
    
});

//GET ROUTES

Route::get('/home/haha',array('uses'=>'Larry\Frontend\Controllers\HomeController@getIndex'));
Route::get('/home/dashboard/user',array('uses'=>'Larry\Frontend\Controllers\HomeController@getUserDash'));




Route::get('/events/add',  array ('uses'=>'Funkies\Events\Controllers\EventsController@getIndex'));
Route::get('/events/list',  array ('uses'=>'Funkies\Events\Controllers\EventsController@getList'));
Route::get('/events/create',  array ('uses'=>'Funkies\Events\Controllers\EventsController@getCreate'));
Route::get('/events/edit',  array ('uses'=>'Funkies\Events\Controllers\EventsController@getEdit'));
Route::get('/events/profile',  array ('uses'=>'Funkies\Events\Controllers\EventsController@getProfile'));
Route::get('/events/profile/edit',  array ('uses'=>'Funkies\Events\Controllers\EventsController@getEditprofile'));

//GET MANAGE ROUTES:
Route::get('/events/manage/{id}/{name}',  array ('uses'=>'Funkies\Events\Controllers\EventsController@getManage'));
Route::get('/events/invitations/get/{id}',  array ('uses'=>'Funkies\Events\Controllers\EventsController@getInvitations'));
Route::get('/events/view/{id}',  array ('uses'=>'Funkies\Events\Controllers\EventsController@getEvent'));




//POST ROUTES:
Route::post('/events/add/basic_details',  array ('uses'=>'Funkies\Events\Controllers\EventsController@postBasic_event'));
Route::post('/events/add/event_settings',  array ('uses'=>'Funkies\Events\Controllers\EventsController@postEvent_settings'));
Route::post('/events/add/invitations',  array ('uses'=>'Funkies\Events\Controllers\EventsController@postInvitations'));
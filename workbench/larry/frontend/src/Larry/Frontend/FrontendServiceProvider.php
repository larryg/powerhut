<?php

namespace Larry\Frontend;

use Illuminate\Support\ServiceProvider;

class FrontendServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot() {
        $this->package('larry/frontend');
        \Basset::collection('namer', function($collection) {
                    $collection->stylesheet('bootstrap.css');
//                $collection->stylesheet('/assets/stylesheets/theme.css');
//                $collection->stylesheet('/assets/stylesheets/index.css');
                });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
//        
//                \Basset::collection('namer', function($collection) {
//                    $collection->stylesheet('bootstrap.css');
////                $collection->stylesheet('/assets/stylesheets/theme.css');
////                $collection->stylesheet('/assets/stylesheets/index.css');
//                });
        //
        include __DIR__ . "/../../routes.php";
    }

//     public function boot() {
//        $this->package('larry/frontend');
//    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides() {
        return array();
    }

}
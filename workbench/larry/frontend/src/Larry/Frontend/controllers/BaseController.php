<?php

/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
*
* @author larry
**/ 
//namespace Funkies\Events;

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}
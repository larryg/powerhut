<!DOCTYPE html>
<html>
<head>
	<title>Mulika KPLC | Home...</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
    <!-- Styles -->
<!--    <link href="css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/theme.css">
    <link rel="stylesheet" href="css/index.css" type="text/css" media="screen" />-->
    <link rel="stylesheet" href="{{ asset('/assets/stylesheets/bootstrap.css') }}">
     <link rel="stylesheet" href="{{ asset('/assets/stylesheets/theme.css') }}">
    
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="css/lib/animate.css" media="screen, projection">    
<script src={{asset('/assets/javascripts/jquery-1.10.1.js')}} ></script>
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

  

 @yield('content')

    <!-- starts footer -->

    <!-- Scripts -->

    
    
    <script src={{asset('/assets/javascripts/bootstrap.min.js')}} ></script>
    <script src={{asset('/assets/javascripts/theme.js')}}></script>

    <script type="text/javascript" src="{{asset('/assets/javascripts/index-slider.js')}}"></script>	
    
   
    
    
</body>
</html>
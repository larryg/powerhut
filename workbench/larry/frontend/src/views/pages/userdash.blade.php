@extends('frontend::templates.template')
@section('content')  

<style>
    #mymap{
        width:100%;
        height: 250px;
        margin-bottom:20px;
        margin-top: -20px;
    }
    #mymap img {
    max-width: none;
}
body{
    padding-top:40px;
}
    
</style>

<script type="text/javascript"
src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en">
    
  </script>
  
  <script type="text/javascript" src="/assets/gmap/gmap3.js"></script>
   <script>
      
     $(document).ready(function(){
      
       $("#mymap").gmap3({map:{
            options:{
              center:[-1.299593, 36.765374],
              zoom: 13
            }
          },
          marker:{
            values:[
              {latLng:[-1.299593, 36.765374], data:"88 mph! Hackathon!"},
             ],
            options:{
              draggable: false
            },
            events:{
              mouseover: function(marker, event, context){
                var map = $(this).gmap3("get"),
                  infowindow = $(this).gmap3({get:{name:"infowindow"}});
                if (infowindow){
                  infowindow.open(map, marker);
                  infowindow.setContent(context.data);
                } else {
                  $(this).gmap3({
                    infowindow:{
                      anchor:marker, 
                      options:{content: context.data}
                    }
                  });
                }
              },
              mouseout: function(){
                var infowindow = $(this).gmap3({get:{name:"infowindow"}});
                if (infowindow){
                  infowindow.close();
                }
              }
            }
          }
        });
     });
      
      </script>

<link rel="stylesheet" href="{{ asset('/assets/stylesheets/blogpost.css') }}">
<link href="/assets/nvd3/src/nv.d3.css" rel="stylesheet" type="text/css">
<script src="/assets/nvd3/lib/d3.v2.js"></script>
<script src="/assets/nvd3/nv.d3.js"></script>
<script src="/assets/nvd3/src/tooltip.js"></script>
<script src="/assets/nvd3/src/utils.js"></script>
<script src="/assets/nvd3/src/models/legend.js"></script>
<script src="/assets/nvd3/src/models/axis.js"></script>
<script src="/assets/nvd3/src/models/scatter.js"></script>
<script src="/assets/nvd3/src/models/line.js"></script>
<script src="/assets/nvd3/src/models/lineChart.js"></script>
<script>
    // Wrapping in nv.addGraph allows for '0 timeout render', stores rendered charts in nv.graphs, and may do more in the future... it's NOT required
var chart;

nv.addGraph(function() {
  chart = nv.models.lineChart();

  chart
      .x(function(d,i) { return i })


  chart.xAxis // chart sub-models (ie. xAxis, yAxis, etc) when accessed directly, return themselves, not the parent chart, so need to chain separately
      .tickFormat(d3.format(',.1f'));

  chart.yAxis
      .axisLabel('Voltage (v)')
      .tickFormat(d3.format(',.2f'));

  d3.select('#chart1 svg')
      //.datum([]) //for testing noData
      .datum(sinAndCos())
    .transition().duration(500)
      .call(chart);

  //TODO: Figure out a good way to do this automatically
  nv.utils.windowResize(chart.update);
  //nv.utils.windowResize(function() { d3.select('#chart1 svg').call(chart) });

  chart.dispatch.on('stateChange', function(e) { nv.log('New State:', JSON.stringify(e)); });

  return chart;
});



function sinAndCos() {
  var sin = [],
      cos = [];

  for (var i = 0; i < 100; i++) {
    sin.push({x: i, y: i % 10 == 5 ? null : Math.sin(i/10) }); //the nulls are to show how defined works
    cos.push({x: i, y: .5 * Math.cos(i/10)});
  }

  return [
    {
      area: true,
      values: sin,
      key: "Sine Wave",
      color: "#ff7f0e"
    },
    {
      values: cos,
      key: "Cosine Wave",
      color: "#2ca02c"
    }
  ];
}


</script>
<body>
 @include('frontend::templates.header')

    <div id="blog_post">
        <div class="container">
            <div class="section_header">
                <h3>Your Dashboard</h3>

            </div>

            <div class="row">
                <div class="span12">
                    <div id="mymap" class="img-polaroid">
                        
                    </div>
                </div>
                <div class="span8 pull-right">
<!--                    <img class="post_pic" src="/img/blog_post.jpg" />-->
                    <h3>This Month's Consumption:</h3>
                     <div id="chart1">
    <svg style="height: 300px;"></svg>
  </div>
                    

                    <div class="post_content">
                        <h3>Your Account</h3>
                        <span class="date">Wed, 12 Dec.</span>
                        <p>
                            There are many variations of passages of Lorem Ipsum available, injected generators on the  embarrassing hidden in the middle all the dictionary of  randomised words which don’t look even slightly distracted by these distribution of letters, as opposed to using ‘Content here, content here making it look like readable English. Many desktop publishing packages and web page editors.
                        </p>

                        <p>
                            There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don’t look even slightly believable. If you are anything embarrassing hidden in the middle of text. All the Lorem Ipsum necessary, making this the first true generator on the Internet. It uses an dictionary of over 200 Latin words, combined with a handful of non-characteristic words There are many variations necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Ipsum available, injected generators on the  embarrassing hidden distribution of letters, as opposed to using ‘Content here, content here making it look like readable English. Many desktop publishing packages and web page editors.</p>

                        <p>
                            There are many variations of passages of Lorem Ipsum available, injected generators on the  embarrassing hidden in the middle all the dictionary of  randomised words which don’t look even slightly distracted by these distribution of letters, as opposed to using ‘Content here, content here making it look like readable English. Many desktop publishing packages and web page editors.</p>
                        
                        <div class="author_box">
                            <div class="author">Alejandra Galvan</div>
                            <div class="area">Creative Director</div>
                        </div>
                    </div>

                
                </div>
                
                <!-- SideBar -->
                <div class="span3 sidebar pull-left">
                    <div class="box">
                        <div class="imgbox">
                            <img src="/img/male.jpg" width="100" class="img-rounded">
                        </div>
                        <div class="sidebar_header">
                            <h5>Jack Bauer</h5>
                        </div>
                        <strong>DETAILS:</strong>
                        <ul class="sidebar_menu">
                            <ul>
                                <li><a href="#"><b>Current Usage</b></a> <span class="span1 pull-right">2.5555</span></li>
                                <li><a href="#"><b>Last Bill</b></a> <span class="span1 pull-right">12.002</span></li>
                                
                                <li><a href="#"><b>Bill Number</b></a><span class="span1 pull-right">300390</span></li>
                                <li><a href="#">Current Rate/KW</a></li>
<!--                                <li><a href="#">Packaging   </a></li>-->
                            </ul>
                        </ul>
                    </div>
                    
                    <div class="box box2">
                        <div class="sidebar_header">
                            <h4>Recent Bills</h4>
                        </div>

                        <div class="recent">
                            <span class="date">Dec. 2012</span>
                            <p>Kshs. 3500</p>
                        </div>
                        <div class="recent">
                            <span class="date">Jan 2013</span>
                            <p>Kshs. 3500</p>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
 
 


 @include('frontend::templates.footer')
    </body>
 @stop